﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarBookApi.Models;
using CarBookApi.Models.DB;
using CarBookApi.Modules;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace CarBookApi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class RecordsController : Controller
    {
        private IConfiguration m_Configuration;

        public RecordsController(IConfiguration configuration)
        {
            // m_AppConfiguration = appConfiguration;
            m_Configuration = configuration;
        }

        [HttpHead]
        [HttpGet]
        [Route("{carId}")]
        public async Task<IActionResult> GetRecords([FromRoute]int carId)
        {
            var header = Request.Headers["Authorization"].ToString();
            if (header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");
                var um = new UserManager(m_Configuration);
                var user = await um.GetUserFromTokenAsync(token);

                if (user != null)
                {
                    List<RecordModel> recordsList = null;
                    using (var dbContex = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]))
                    {
                        var car = await dbContex.Car.FirstOrDefaultAsync(x => x.User.Id == user.Id && x.Id == carId);

                        if(car != null)
                        {
                            recordsList = new List<RecordModel>();
                            var records = await dbContex.Record.Include(u => u.User).Include(r => r.RecordType).Where(x => x.User.Id == user.Id && x.Car.Id == carId).ToListAsync();
                            foreach (var record in records)
                            {
                                recordsList.Add(new RecordModel(record));
                            }
                        }
                    }

                    if (recordsList != null)
                        return Ok(recordsList.Count > 0 ? recordsList : null);
                    else
                        return BadRequest("Car not exists");

                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpHead]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var header = Request.Headers["Authorization"].ToString();
            if (header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");
                var um = new UserManager(m_Configuration);
                var user = await um.GetUserFromTokenAsync(token);

                if (user != null)
                {
                    var dbContex = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
                    var records = await dbContex.Record.Include(r => r.RecordType).Include(u => u.User).Include(c => c.Car).Where(x => x.User.Id == user.Id).ToListAsync();

                    var recordsList = new List<RecordModel>();

                    foreach (var record in records)
                    {
                        recordsList.Add(new RecordModel(record));
                    }

                    return Ok(recordsList);
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]RecordModel recordModel)
        {
            var header = Request.Headers["Authorization"].ToString();
            if (header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");
                var um = new UserManager(m_Configuration);
                var user = await um.GetUserFromTokenAsync(token);

                if (user != null && recordModel != null)
                {
                    if (recordModel.Id > 0)
                    {
                        Record recordDB = null;
                        using (var context = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]))
                        {
                            recordDB = await context.Record.FirstOrDefaultAsync(x => x.Id == recordModel.Id);

                            if (recordDB != null)
                            {
                                recordDB.RecordType = recordModel.RecordType;
                                recordDB.User = await context.User.FirstOrDefaultAsync(x => x.Id == user.Id);
                                ////recordDB.
                                //recordDB.Vendor = carModel.Vendor;
                                //recordDB.Year = carModel.Year;
                                //recordDB.EngineVolume = carModel.EngineVolume;
                                //recordDB.Color = carModel.Color;

                                await context.SaveChangesAsync();
                            }
                        }

                        return Ok();
                    }
                    else
                    {
                        using (var context = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]))
                        {
                            //var carDb = carModel.ToCar(await context.User.FirstOrDefaultAsync(x => x.Id == user.Id));

                            //await context.Car.AddAsync(carDb);
                            //await context.SaveChangesAsync();

                            //return Ok(new CarModel(carDb));
                            return Ok();
                        }
                    }
                }
                else
                {
                    if (recordModel == null)
                        return BadRequest();
                    else
                        return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}