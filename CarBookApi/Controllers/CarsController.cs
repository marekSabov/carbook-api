﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarBookApi.Models;
using CarBookApi.Models.DB;
using CarBookApi.Modules;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace CarBookApi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CarsController : Controller
    {
        private IConfiguration m_Configuration;

        public CarsController(IConfiguration configuration)
        {
            // m_AppConfiguration = appConfiguration;
            m_Configuration = configuration;
        }

        [HttpHead]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var header = Request.Headers["Authorization"].ToString();
            if (header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");                
                var um = new UserManager(m_Configuration);
                var user = await um.GetUserFromTokenAsync(token);

                if(user != null)
                {
                    var context = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
                    var cars = await context.Car.Include(u => u.User).Where(x => x.User.Id == user.Id).ToListAsync();

                    var carsList = new List<CarModel>();

                    foreach (var car in cars)
                    {
                        car.User = user;
                        carsList.Add(new CarModel(car));
                    }

                    return Ok(carsList);
                }
                else
                {
                    return BadRequest();
                }

            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CarModel carModel)
        {
            var header = Request.Headers["Authorization"].ToString();
            if (header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");
                var um = new UserManager(m_Configuration);
                var user = await um.GetUserFromTokenAsync(token);

                if(user != null && carModel != null)
                {
                    if(carModel.Id > 0)
                    {
                        Car carDB = null;
                        using (var context = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]))
                        {
                            carDB = await context.Car.FirstOrDefaultAsync(x => x.Id == carModel.Id);

                            if(carDB != null)
                            {
                                carDB.Type = carModel.Type;
                                carDB.User = await context.User.FirstOrDefaultAsync(x => x.Id == user.Id);
                                carDB.Vendor = carModel.Vendor;
                                carDB.Year = carModel.Year;
                                carDB.EngineVolume = carModel.EngineVolume;
                                carDB.Color = carModel.Color;

                                await context.SaveChangesAsync();
                            }
                        }

                        return Ok();
                    }
                    else
                    {
                        using (var context = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]))
                        {
                            var carDb = carModel.ToCar(await context.User.FirstOrDefaultAsync(x => x.Id == user.Id));

                            await context.Car.AddAsync(carDb);
                            await context.SaveChangesAsync();

                            return Ok(new CarModel(carDb));
                        }
                    }
                }
                else
                {
                    if (carModel == null)
                        return BadRequest();
                    else
                        return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpDelete]
        [Route("{carId}")]
        public async Task<IActionResult> Delete([FromHeader] int carId)
        {
            var header = Request.Headers["Authorization"].ToString();
            if (header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");
                var um = new UserManager(m_Configuration);
                var user = await um.GetUserFromTokenAsync(token);

                if(user != null && carId > 0)
                {
                    using (var context = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]))
                    {
                        var car = await context.Car.FirstOrDefaultAsync(x => x.Id == carId);

                        if(car != null)
                        {
                            context.Car.Remove(car);
                            await context.SaveChangesAsync();

                            return NoContent();
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                }
                else
                {
                    if (carId > 0)
                        return Unauthorized();
                    else
                        return BadRequest();
                }
            }
            else
            {
                return Unauthorized();
            }


                
        }

    }
}