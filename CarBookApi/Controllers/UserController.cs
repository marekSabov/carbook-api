﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarBookApi.Models;
using CarBookApi.Models.DB.Users;
using CarBookApi.Modules;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CarBookApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IConfiguration m_Configuration;

        public UserController (IConfiguration configuration)
        {
            m_Configuration = configuration;
        }

        [HttpHead]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var header = Request.Headers["Authorization"].ToString();
            if(header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");
                var userManager = new UserManager(m_Configuration);
                var userFromDB = await userManager.GetUserFromTokenAsync(token);

                if(userFromDB != null)
                {
                    var user = new UserModel(userFromDB);

                    return Ok(user);
                }
                else
                {
                    return BadRequest("User not found");
                }

                
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]UserModel user)
        {
            var userManager = new UserManager(m_Configuration);

            var header = Request.Headers["Authorization"].ToString();
            if (header != null && header.StartsWith("Bearer"))
            {
                var token = header.Replace("Bearer ", "");
                

                bool updated = await userManager.UpdateUserAsync(user);

                if (updated)
                {
                    return Created("/api/user",user);
                }
                else
                {
                    return BadRequest("User not found.");
                }

            }
            else
            {
                var created = await userManager.CreateUserAsync(user);

                if (created)
                    return Created("/api/user", user);
                else
                    return BadRequest();
            }
                
        }
    }
}