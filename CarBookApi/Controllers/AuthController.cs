﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CarBookApi.Models;
using CarBookApi.Modules;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CarBookApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private IConfiguration m_Configuration;

        public AuthController(IConfiguration configuration)
        {
            m_Configuration = configuration;
        }

        [HttpPost("token")]
        public async Task<IActionResult> Token()
        {
            var header = Request.Headers["Authorization"];
            
            if(header.ToString().StartsWith("Basic"))
            {
                var creadValue = header.ToString().Replace("Basic ", "");
                var credentailsEncoded = Encoding.UTF8.GetString(Convert.FromBase64String(creadValue));
                var credentials = credentailsEncoded.Split(":");

                var userManager = new UserManager(m_Configuration);
                var token = await userManager.GenerateTokenAsync(credentials[0], credentials[1]);

                if (!String.IsNullOrEmpty(token))
                    return Ok(token);
                else
                    return BadRequest("Wrong credentails.");
               
            }
            else
            {
                return BadRequest();
            }
        }
    }
}