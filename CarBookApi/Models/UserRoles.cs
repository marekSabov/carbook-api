﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models.Users
{
    public enum UserRoles
    {
        USER = 1,
        ADMIN = 2,
    }
}
