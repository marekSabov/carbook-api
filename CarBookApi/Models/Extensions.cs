﻿using CarBookApi.Models.DB;
using CarBookApi.Models.DB.Users;
using CarBookApi.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models
{
    public static class Extensions
    {
        //public static UserModelOut ToUserModelOut(this User user)
        //{
        //    return new UserModelOut
        //    {
        //        Id = user.Id,
        //        Email = user.Email,
        //        Password = user.Password,
        //        UserName = user.UserName
        //    };
        //}

        public static User ToUser(this UserModel user, UserRole role)
        {
            if (user != null)
            {
                return new User
                {
                    Id = user.Id,
                    Username = user.Username,
                    Password = user.Password,
                    Email = user.Email,
                    LastLogin = user.LastLogin,
                    Role = role
                };
            }
            else
            {
                return null;
            }
        }

        public static Car ToCar(this CarModel car, User user)
        {
            if(car != null)
            {

                return new Car
                {
                    Id = car.Id,
                    User = user,
                    Color = car.Color,
                    EngineVolume = car.EngineVolume,
                    Type = car.Type,
                    Vendor = car.Vendor,
                    Year = car.Year
                };
            }
            else
            {
                return null;
            }
        }

        public static Record ToRecord(this RecordModel record, User user, Car car)
        {
            if(record != null)
            {
                return new Record
                {
                    Id = record.Id,
                    User = user,
                    Car = car,
                    DateTime = record.DateTime,
                    RecordText = record.RecordText,
                    RecordType = record.RecordType
                };
            }
            else
            {
                return null;
            }
        }

        //public static RecordModelOut ToRecordModelOut(this Record record)
        //{
        //    return new RecordModelOut
        //    {
        //        CarId = record.Car.Id,
        //        DateTime = record.DateTime,
        //        Id = record.Id,
        //        RecordText = record.RecordText,
        //        RecordType = record.RecordType.Id,
        //        UserId = record.User.Id
        //    };
        //}

        //public static Record ToRecords(this RecordModelOut record)
        //{
        //    return new Record
        //    {
        //        CarId = record.Ca,
        //        DateTime = record.DateTime,
        //        Id = record.Id,
        //        RecordText = record.RecordText,
        //        RecordType = record.RecordType,
        //        UserId = record.UserId
        //    };
        //}
    }
}
