﻿using CarBookApi.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models
{
    public class RecordModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CarId { get; set; }
        public DateTime DateTime { get; set; }
        public RecordType RecordType { get; set; }
        public string RecordText { get; set; }

        public RecordModel(Record record)
        {
            Id = record.Id;
            UserId = record.User.Id;
            CarId = record.Car.Id;
            DateTime = record.DateTime;
            RecordType = record.RecordType;
            RecordText = record.RecordText;
        }
    }
}
