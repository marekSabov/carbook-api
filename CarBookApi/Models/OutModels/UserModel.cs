﻿using CarBookApi.Models.DB.Users;
using CarBookApi.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime LastLogin { get; set; }
        public string Role { get; set; }

        public UserModel(User user)
        {
            Id = user.Id;
            Username = user.Username;
            Password = user.Password;
            Email = user.Email;
            LastLogin = user.LastLogin;
            Role = user.Role == null ? "no_role" : user.Role.Role;           
        }

        public UserModel()
        {
           
        }


    }
}
