﻿using CarBookApi.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models
{
    public class CarModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public string Vendor { get; set; }
        public int Year { get; set; }
        public int EngineVolume { get; set; }

        public CarModel() { }

        public CarModel(Car car)
        {
            Id = car.Id;
            Type = car.Type;
            Color = car.Color;
            Vendor = car.Vendor;
            Year = car.Year;
            EngineVolume = car.EngineVolume;
        }
    }
}
