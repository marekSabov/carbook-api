﻿using CarBookApi.Models.DB.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models.DB
{
    public class Car
    {
        public int Id { get; set; }
        public User User { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public string Vendor { get; set; }
        public int Year { get; set; }
        public int EngineVolume { get; set; }
    }
}
