﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models.DB
{
    public class RecordType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
