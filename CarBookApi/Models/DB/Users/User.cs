﻿using CarBookApi.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models.DB.Users
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime LastLogin { get; set; }
        public UserRole Role { get; set; }
        public string Token { get; set; }

        //public ICollection<Car> Cars { get; set; }
    }
}
