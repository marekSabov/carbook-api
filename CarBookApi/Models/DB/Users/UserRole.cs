﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models.DB.Users
{
    public class UserRole
    {
        public int Id { get; set; }
        public string Role{ get; set; }

        public UserRole(int id, string role)
        {
            Id = id;
            Role = role;
        }

        public UserRole()
        {
        }
    }
}
