﻿using CarBookApi.Models.DB.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models.DB
{
    public class CarBookDbContext : DbContext
    {
        public DbSet<Car> Car { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<Record> Record { get; set; }
        public DbSet<RecordType> RecordType { get; set; }


        private string m_ConnectionString;

        public CarBookDbContext(string connectionString)
        {
            m_ConnectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(m_ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Users
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Username).IsRequired();
                entity.Property(e => e.Password).IsRequired();
                entity.HasOne(u => u.Role);
                //entity.HasMany(c => c.Cars);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Role).IsRequired();
            });
            #endregion

            modelBuilder.Entity<Car>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Type).IsRequired();
                entity.HasOne(u => u.User);
            });

            modelBuilder.Entity<RecordType>(entity =>
            {
                entity.HasKey(e => e.Id);
            });

            modelBuilder.Entity<Record>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasOne(u => u.User);
                entity.HasOne(c => c.Car);
                entity.HasOne(r => r.RecordType);
            });

        }
    }
}
