﻿using CarBookApi.Models.DB.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models.DB
{
    public class Record
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Car Car { get; set; }
        public DateTime DateTime { get; set; }
        public RecordType RecordType { get; set; }
        public string RecordText { get; set; }
    }
}
