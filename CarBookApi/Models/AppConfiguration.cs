﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models
{
    public class AppConfiguration
    {
        public string MySqlConnectionString { get; set; }
        public int AppPort { get; set; }
        public AuthConfig AuthConfig { get; set; }

    }

    public class AuthConfig
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string AppToken { get; set; }
        public int TokenExpire { get; set; }
    }
}
