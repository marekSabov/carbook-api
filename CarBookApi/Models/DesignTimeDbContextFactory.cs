﻿using CarBookApi.Models.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarBookApi.Models
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CarBookDbContext>
    {
        public CarBookDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<CarBookDbContext>();
            var connectionString = configuration["AppConfiguration:MySqlConnectionString"];
            builder.UseSqlServer(connectionString);
            return new CarBookDbContext(connectionString);
        }
    }
}
