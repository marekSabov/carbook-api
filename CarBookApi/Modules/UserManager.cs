﻿using CarBookApi.Models;
using CarBookApi.Models.DB;
using CarBookApi.Models.DB.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CarBookApi.Modules
{
    public class UserManager
    {
        private IConfiguration m_Configuration;

        public UserManager(IConfiguration configuration)
        {
            m_Configuration = configuration;
        }

        public async Task<int?> GetUserIdFromUsernameAsync(string username)
        {
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var user = await dbContext.User.FirstOrDefaultAsync(x => x.Username == username);           

            if (user != null)
            {
                //if (user.Role == null)
                //{
                //    var userRole = await dbContext.UserRole.FromSql($"call get_user_role({user.Id})").FirstOrDefaultAsync();
                //    user.Role = userRole;
                //}

                return user.Id;
            }
            else
            {
                return null;
            }
        }

        public async Task<User> GetUserFromTokenAsync(string token)
        {
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var user = await dbContext.User.Include(u => u.Role).FirstOrDefaultAsync(x => x.Token == token);

            if (user != null)
            {
                //if (user.Role == null)
                //{
                //    var userRole = await dbContext.UserRole.FromSql($"call get_user_role({user.Id})").FirstOrDefaultAsync();
                //    user.Role = userRole;
                //}


                return user;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> IsCorrectUserPassAsync(int userId, string password)
        {
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var user = await dbContext.User.FirstOrDefaultAsync(x => x.Id == userId);

            if (user != null && user.Password == password)
                return true;
            else
                return false;
        }

        public async Task<bool> IsCredentialsCorrenctAsync(string username, string password)
        {
            var userId = await GetUserIdFromUsernameAsync(username);

            if(userId != null)
            {
                return await IsCorrectUserPassAsync((int)userId, password);
            }
            else
            {
                return false;
            }
        }

        public async Task SaveTokenAsync(string username, string token)
        {
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var user = await dbContext.User.FirstOrDefaultAsync(x => x.Username == username);
            user.Token = token;
            await dbContext.SaveChangesAsync();
        }

        public async Task<string> GenerateTokenAsync(string username, string password)
        {
            if (await IsCredentialsCorrenctAsync(username, password))
            {
                var claimsData = new[] { new Claim(ClaimTypes.Name, "username") };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(m_Configuration["AppConfiguration:AuthConfig:AppToken"]));
                var signInCreds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

                var token = new JwtSecurityToken(
                    issuer: m_Configuration["AppConfiguration:AuthConfig:Issuer"],
                    audience: m_Configuration["AppConfiguration:AuthConfig:Audience"],
                    expires: DateTime.Now.AddMinutes(Convert.ToDouble(m_Configuration["AppConfiguration:AuthConfig:TokenExpire"])),
                    claims: claimsData,
                    signingCredentials: signInCreds
                    );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                await SaveTokenAsync(username, tokenString);
                return tokenString;
            }
            else
            {
                return "";
            }
        }

        public async Task<User> GetUserAsync(int id)
        {
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var user = await dbContext.User.Include(u => u.Role).FirstOrDefaultAsync(x => x.Id == id);

            if(user!= null)
            {
                //if (user.Role == null)
                //{
                //    var userRole = await dbContext.UserRole.FromSql($"call get_user_role({user.Id})").FirstOrDefaultAsync();
                //    user.Role = userRole;
                //}

                return user;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> UpdateUserAsync(UserModel user)
        {
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var role = await dbContext.UserRole.FirstOrDefaultAsync(x => x.Role == user.Role);
            var dbUser = user.ToUser(role);
            return await UpdateUserAsync(dbUser);
        }

        public async Task<bool> UpdateUserAsync(User user)
        {
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var userFromDB = await dbContext.User.FirstOrDefaultAsync(x => x.Id == user.Id);

            if (userFromDB != null)
            {
                userFromDB.Email = user.Email;
                userFromDB.Password = user.Password;
                userFromDB.Username = user.Username;

                if (user.Role != null)
                    user.Role = await dbContext.UserRole.FirstOrDefaultAsync(x => x.Id == user.Role.Id);

                userFromDB.Role = user.Role ?? await dbContext.UserRole.FirstOrDefaultAsync(x => x.Role == "user");

                await dbContext.SaveChangesAsync();

                return true;
            }
            else
            {
                return false;
            }


        }

        public async Task<bool> CreateUserAsync(UserModel user)
        {
            
            var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
            var role = await dbContext.UserRole.FirstOrDefaultAsync(x => x.Role == user.Role);
            var dbUser = user.ToUser(role);
            return await CreateUserAsync(dbUser);
        }

        public async Task<bool> CreateUserAsync(User user)
        {
            try
            {
                var dbContext = new CarBookDbContext(m_Configuration["AppConfiguration:MySqlConnectionString"]);
                
                if(user.Role != null)
                    user.Role = await dbContext.UserRole.FirstOrDefaultAsync(x => x.Id == user.Role.Id);

                user.Role = user.Role?? await dbContext.UserRole.FirstOrDefaultAsync(x => x.Role == "user");
                user.LastLogin = DateTime.Now;
                await dbContext.User.AddAsync(user);
                await dbContext.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
